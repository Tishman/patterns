
/// Фабричный метод — это порождающий паттерн проектирования, который определяет общий интерфейс для
/// создания объектов в суперклассе, позволяя подклассам изменять тип создаваемых объектов.

protocol Transport {
	var transportInfo: String { get set }
	func deliver()
}

protocol Logistic {
	func createTransport(type: TransportType) -> Transport
}

struct Truck: Transport {
	var transportInfo: String
	
	func deliver() {
		print("Truck starts delivering.")
	}
}

struct Ship: Transport {
	var transportInfo: String
	
	func deliver() {
		print("Ship starts delivering.")
	}
}

struct Train: Transport {
	var transportInfo: String
	
	func deliver() {
		print("Train starts delivering.")
	}
}

struct Airplain: Transport {
	var transportInfo: String
	
	func deliver() {
		print("Airplain starts delivering.")
	}
}

enum TransportType {
	case truck
	case ship
	case train
	case airplain
}

final class LogisticACompany: Logistic {
	func createTransport(type: TransportType) -> Transport {
		switch type {
		case .truck:
			return Truck(transportInfo: "It's an aCompany's truck!")
		case .ship:
			return Ship(transportInfo: "It's an aCompany's ship!")
		case .train:
			return Train(transportInfo: "It's an aCompany's train!")
		case .airplain:
			return Airplain(transportInfo: "It's an aCompany's airplain!")
		}
	}
}

final class LogisticBCompany: Logistic {
	func createTransport(type: TransportType) -> Transport {
		switch type {
		case .truck:
			return Truck(transportInfo: "It's a bCompany's truck!")
		case .ship:
			return Ship(transportInfo: "It's a bCompany's ship!")
		case .train:
			return Train(transportInfo: "It's a bCompany's train!")
		case .airplain:
			return Airplain(transportInfo: "It's a bCompany's airplain!")
		}
	}
}

let aCompany: Logistic = LogisticACompany()
let bCompany: Logistic = LogisticBCompany()

let truck = aCompany.createTransport(type: .truck)
let airplane = bCompany.createTransport(type: .airplain)
let train = aCompany.createTransport(type: .train)
let ship = bCompany.createTransport(type: .ship)

[truck, airplane, train, ship].forEach {
	print($0.transportInfo)
	$0.deliver()
	print()
}
